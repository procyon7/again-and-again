
# coding: utf-8

# In[1]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv




# In[2]:


#P1: total number of clicks during session
file = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/train-all.csv"
df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',') 
lines = df.iloc[:,0:4].values

click_count = {}

for i in range(lines.shape[0]):
    if lines[i][0] not in click_count:
        click_count[lines[i][0]] = 1
    else:
        val = click_count.get(lines[i][0])
        val += 1
        click_count[lines[i][0]] = val

#session ID and total clicks in that session
with open('total-click-counts.csv', 'w') as out:
    writer = csv.writer(out)
    for key, value in click_count.items():
       writer.writerow([key, value])

