
# coding: utf-8

# In[18]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv
import collections
import iso8601
import datetime


# In[25]:


f = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/only-multiples.csv"
pf = pd.read_csv(filepath_or_buffer = f, header=None, sep=',') 
lines_multi = pf.iloc[:,0:4].values


# In[65]:


sess_item = {}

for i in range(lines_multi.shape[0]): 
    inp = lines_multi[i][0]
    val = [lines_multi[i][1],lines_multi[i][2]]
    if inp in sess_item:
        sess_item[inp].append(val) 
    else:
        sess_item[inp] = [val]


# In[66]:


date_item = np.empty([0,2], dtype=object)


# In[67]:


for key in sess_item:
    consec_dur = []

    date_item = sess_item.get(key)
    date_item.sort()
    for i in range(len(date_item)-1):
        dur = 0
        if date_item[i][1] == date_item[i+1][1]:
            st = iso8601.parse_date(date_item[i][0])
            end = iso8601.parse_date(date_item[i+1][0])
            dur = end-st
        if dur != 0:
            consec_dur.append(dur)
            
    if len(consec_dur) != 0:     
        sess_item[key] = max(consec_dur)
    else:
        zero = datetime.datetime.now()
        zero -= zero
        sess_item[key] = zero


# In[68]:


f1 = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/p3-p4-p5-p6-p10.csv"
df = pd.read_csv(filepath_or_buffer = f1, header=None, sep=',') 
lines = df.iloc[:,0:9].values

#to compute average value for maximal number of items the session has clicked on the same item
#I need the number of all sessions

all_sess = {}
for i in range(lines.shape[0]):
    if lines[i][0] not in all_sess:
        all_sess[lines[i][0]] = i


# In[69]:


maximal_dur = datetime.datetime.now()
maximal_dur -= maximal_dur
for key in sess_item:
    maximal_dur += sess_item[key]


# In[70]:


avg_maximal_dur = maximal_dur / len(all_sess)
print(avg_maximal_dur)


# In[71]:


maxima_dura = np.empty([lines.shape[0], 10], dtype=object)
if_zero = datetime.datetime.now()
if_zero -= if_zero
for i in range(lines.shape[0]):
    l = np.array(lines[i])
    if lines[i][0] in sess_item:
        
        if sess_item.get(lines[i][0]) == if_zero:
            y = np.array(avg_maximal_dur)
            maxima_dura[i] = np.append(l, y)
        else:
            y = np.array(sess_item.get(lines[i][0]))
            maxima_dura[i] = np.append(l, y)
    else:
        y = np.array(avg_maximal_dur)
        maxima_dura[i] = np.append(l, y)



# In[72]:


with open('p3-p4-p5-p6-p10-p11.csv', 'w') as out:
    for i in range(maxima_dura.shape[0]):
        data = maxima_dura[i].tolist()
        writer = csv.writer(out)
        writer.writerow(data)

