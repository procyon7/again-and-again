
# coding: utf-8

# In[1]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv
import collections


# In[3]:


f = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/only-multiples.csv"
pf = pd.read_csv(filepath_or_buffer = f, header=None, sep=',') 
lines_multi = pf.iloc[:,0:4].values


# In[93]:


sess_item = {}
for i in range(lines_multi.shape[0]): 
    inp = lines_multi[i][0]
    val = lines_multi[i][2]
    if inp in sess_item:
        sess_item[inp].append(val) 
    else:
        sess_item[inp] = [val]


# In[94]:


l = []
for key in sess_item:
    maximal = 0
    counter = collections.Counter(sess_item.get(key))
    l = list(counter.keys())
    for i in range(len(l)):    
        if counter[l[i]] > 1:
            maximal += 1
    
    sess_item[key] = maximal


# In[2]:


f1 = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/p3-p4-p5-feature.csv"
df = pd.read_csv(filepath_or_buffer = f1, header=None, sep=',') 
lines = df.iloc[:,0:7].values

#to compute average value for maximal number of items the session has clicked on the same item
#I need the number of all sessions

all_sess = {}
for i in range(lines.shape[0]):
    if lines[i][0] not in all_sess:
        all_sess[lines[i][0]] = i


# In[96]:


count = 0
for key in sess_item:
    count += sess_item[key]


# In[97]:


avg_clicked_item = count / len(all_sess)
print(avg_clicked_item)


# In[98]:


avg_clicked = np.empty([lines.shape[0], 8], dtype=object)

for i in range(lines.shape[0]):
    l = np.array(lines[i])    
    if lines[i][0] in sess_item:
        if sess_item.get(lines[i][0]) == 0:
            y = np.array(avg_clicked_item)
            avg_clicked[i] = np.append(l, y)
        else:
            y = np.array(sess_item.get(lines[i][0]))
            avg_clicked[i] = np.append(l, y)
    else:
        y = np.array(avg_clicked_item)
        avg_clicked[i] = np.append(l, y)


# In[99]:


with open('p3-p4-p5-p6.csv', 'w') as out:
    for i in range(avg_clicked.shape[0]):
        data = avg_clicked[i].tolist()
        writer = csv.writer(out)
        writer.writerow(data)

