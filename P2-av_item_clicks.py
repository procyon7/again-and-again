
# coding: utf-8

# In[1]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv


# In[2]:


#P2: average number of clicks per item
file = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/train-all.csv"
df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',') 
lines = df.iloc[:,0:3].values
item_clicks = {}
sess_dict = {}


for i in range(lines.shape[0]):
    if lines[i][0] not in sess_dict:
        sess_dict[lines[i][0]] = i
    

for i in range(lines.shape[0]):
    if lines[i][2] not in item_clicks:
        item_clicks[lines[i][2]] = 1
    else:
        val = item_clicks.get(lines[i][2])
        val += 1
        item_clicks[lines[i][2]] = val

        
total_sess = len(sess_dict)
print(len(sess_dict))

for key in item_clicks:
    val = item_clicks.get(key)
    val = val / total_sess
    item_clicks[key] = val
    
with open('av_item_clicks.csv', 'w') as out:
    writer = csv.writer(out)
    for key, value in item_clicks.items():
       writer.writerow([key, value])

