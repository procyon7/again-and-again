
# coding: utf-8

# In[4]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv


# In[5]:


class Keey:
    def __init__(self, session_id, time_stamp, item_id, item_category):
        self.session_id   = session_id
        self.time_stamp = time_stamp
        self.item_id     = item_id
        self.item_category  = item_category
    def converToList(self):
        return [self.session_id, self.time_stamp, self.item_id, self.item_category]


# In[3]:


def read_buys_from_file(path):
    f1 = path + '/yoochoose-buys.dat'
    bf = pd.read_csv(filepath_or_buffer = f1, header=None, sep=',') 
    lines_of_buy = bf.iloc[:,0:5].values 
    buy_session_dict = {} #dictionary to keep unique session IDs 
    c1 = 0 
    
    for i in range(lines_of_buy.shape[0]): 
        if lines_of_buy[i][0] not in buy_session_dict: 
            buy_session_dict[lines_of_buy[i][0]] = c1 
            c1 += 1
    print("read from file")
    return buy_session_dict


# In[12]:


def read_clicks_from_file1(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1    
    
    print("non_buy_session1 dict is created.")
    return non_buy_session_dict


# In[8]:


def read_clicks_from_file2(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=1000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session2 dict is created.")
    return non_buy_session_dict


# In[16]:


def read_clicks_from_file3(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=2000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict: 
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session3 dict is created.")
    return non_buy_session_dict


# In[18]:


def read_clicks_from_file4(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=3000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session4 dict is created.")
    return non_buy_session_dict


# In[20]:


def read_clicks_from_file5(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=4000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session5 dict is created.")
    return non_buy_session_dict


# In[22]:


def read_clicks_from_file6(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=5000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session6 dict is created.")
    return non_buy_session_dict


# In[24]:


def read_clicks_from_file7(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=6000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session7 dict is created.")
    return non_buy_session_dict


# In[26]:


def read_clicks_from_file8(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=7000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session8 dict is created.")
    return non_buy_session_dict


# In[28]:


def read_clicks_from_file9(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=8000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session9 dict is created.")
    return non_buy_session_dict


# In[30]:


def read_clicks_from_file10(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=9000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session10 dict is created.")
    return non_buy_session_dict


# In[32]:


def read_clicks_from_file11(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=10000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session11 dict is created.")
    return non_buy_session_dict


# In[34]:


def read_clicks_from_file12(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=11000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session12 dict is created.")
    return non_buy_session_dict


# In[36]:


def read_clicks_from_file13(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=12000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session13 dict is created.")
    return non_buy_session_dict


# In[38]:


def read_clicks_from_file14(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=13000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session14 dict is created.")
    return non_buy_session_dict


# In[40]:


def read_clicks_from_file15(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=14000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session15 dict is created.")
    return non_buy_session_dict


# In[42]:


def read_clicks_from_file16(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=15000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session dict16 is created.")
    return non_buy_session_dict


# In[43]:


def read_clicks_from_file17(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=16000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session17 dict is created.")
    return non_buy_session_dict


# In[44]:


def read_clicks_from_file18(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=17000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session18 dict is created.")
    return non_buy_session_dict


# In[45]:


def read_clicks_from_file19(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=18000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session19 dict is created.")
    return non_buy_session_dict


# In[46]:


def read_clicks_from_file20(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=19000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session20 dict is created.")
    return non_buy_session_dict


# In[47]:


def read_clicks_from_file21(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=20000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session21 dict is created.")
    return non_buy_session_dict


# In[48]:


def read_clicks_from_file22(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=21000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session22 dict is created.")
    return non_buy_session_dict


# In[49]:


def read_clicks_from_file23(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=22000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session23 dict is created.")
    return non_buy_session_dict


# In[5]:


def read_clicks_from_file24(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=23000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session24 dict is created.")
    return non_buy_session_dict


# In[8]:


def read_clicks_from_file25(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=24000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session25 dict is created.")
    return non_buy_session_dict


# In[9]:


def read_clicks_from_file26(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=25000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session26 dict is created.")
    return non_buy_session_dict


# In[10]:


def read_clicks_from_file27(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=26000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session27 dict is created.")
    return non_buy_session_dict


# In[11]:


def read_clicks_from_file28(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=27000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session28 dict is created.")
    return non_buy_session_dict


# In[12]:


def read_clicks_from_file29(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=28000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session29 dict is created.")
    return non_buy_session_dict


# In[13]:


def read_clicks_from_file30(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=29000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session30 dict is created.")
    return non_buy_session_dict


# In[14]:


def read_clicks_from_file31(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=30000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session31 dict is created.")
    return non_buy_session_dict


# In[15]:


def read_clicks_from_file32(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=31000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session32 dict is created.")
    return non_buy_session_dict


# In[16]:


def read_clicks_from_file33(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=32000000, nrows=1000000)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(1000000): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session33 dict is created.")
    return non_buy_session_dict


# In[24]:


def read_clicks_from_file34(path, buy_session_dict):
    f2 = path + '/yoochoose-clicks.dat'
    cf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',', skiprows=33000000, nrows=3944)
    lines_of_click = cf.iloc[:,0:4].values 

    #find sessions in lines_of_click, which is in buy_session_dict to remove from clicks.dat
    non_buy_session_dict = {}
    c2 = 0
    for i in range(3944): #range(lines_of_click.shape[0]):
        if lines_of_click[i][0] not in buy_session_dict:
            #print(i)
            at = Keey(lines_of_click[i][0], lines_of_click[i][1],lines_of_click[i][2], lines_of_click[i][3])
            non_buy_session_dict[ at ] = c2
            c2 += 1

    print("non_buy_session34 dict is created.")
    return non_buy_session_dict


# In[3]:


def convertDictToList(taken_dict):
    click_list = []

    for i in taken_dict:
        click_list.append(i.converToList())
    print("click_list is created and its numpy array form will be returned.")
    n_array = np.array(click_list)
    
    return n_array


# In[ ]:


#Dosyayı daha önce main'de yazmıştın. Bu kısmı run etmene gerek yok.
#Yanlışlıkla run etme ihtimaline karşı non-buy.csv değişmesin diye yazdığın dosyanın adını değiştiriyorum.
#Ama senin kullanacağın dosya non-buy.csv

def write_to_non_buy(non_buy_sess_list):
    df = pd.DataFrame(non_buy_sess_list)
    df.to_csv("non-buyaaaaaa.csv", mode='a', header=False, index = False)


# In[20]:


def unique_non_buy_sess(path):
    file = path + '/non-buy.csv'
    nf = pd.read_csv(filepath_or_buffer = file, header=None, sep=',')
    lines_of_non_buy = nf.iloc[:,0:4].values
    nonbuy_sess_dict = {}  #dictionary to keep unique non-buy session IDs 
    dictlist = []
    c3 = 0
    print("entering loop")
    for i in range(lines_of_non_buy.shape[0]):
        if lines_of_non_buy[i][0] not in nonbuy_sess_dict:
            nonbuy_sess_dict[ lines_of_non_buy[i][0] ] = c3
            c3 += 1
            
    for key, value in nonbuy_sess_dict.items():
        temp = [key,value]
        dictlist.append(temp)
    
    return dictlist


# In[16]:


def unique_non_buy_file(unique_dictlist):
    print("writing to file")
    df = pd.DataFrame(unique_dictlist)
    df.to_csv("unique-non-buy.csv", header=False, index = False)


# In[27]:


def split_buy_data(path):
    file = path + "/yoochoose-buys.dat"
    df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',') 
    lines_of_buy = df.iloc[:,0:5].values

    buy_sess_num = lines_of_buy.shape[0]

    eval_num = round(buy_sess_num*0.3)
    rand_num = random.sample(range(0, buy_sess_num), eval_num)
    
    dict1 = {}
    for i in range(len(rand_num)):
        dict1[rand_num[i]] = i

    #rand_num[i] bir satır sayısı. lines_of_buy'da o satıra git ve onu eval-buy'a koy

    with open("train-buys.csv", "a") as out1, open("eval-buys.csv", "a") as out2:

        for i in range(lines_of_buy.shape[0]):
            data = lines_of_buy[i].tolist()
            if i not in dict1:
                writer1 = csv.writer(out1)
                writer1.writerow(data)

            else:
                writer2 = csv.writer(out2)
                writer2.writerow(data)
    #return buy_sess_num   #bunu downsample_nonbuy_data'ya gönder. bu kadar sayıda random clicks seçilecek


# In[29]:


#def downsample_nonbuy_data(path):    #def downsample_nonbuy_data(path, sess_num):  sess_num=1150753
#sess_num'ı ilk read_buys_from_file fonksiyonunda buy_sess_dict ile birlikte döndürebilir miyim?
file = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/non-buy.csv"
df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',')
lines_of_non_buy = df.iloc[:, 0:4].values
rand_num = random.sample(range(0, lines_of_non_buy.shape[0]), 1150753)
dict1 = {}
for i in range(len(rand_num)):
    dict1[rand_num[i]] = i
    
with open("reduced-nonbuy.csv", "a") as out:
    for i in range(lines_of_non_buy.shape[0]):
        data = lines_of_non_buy[i].tolist()
        if i in dict1:
            writer = csv.writer(out)
            writer.writerow(data)
 


# In[30]:


#eğer downsample_nonbuy_data fonksiyonundan file döndürüp bu fonksiyona gönderebilirsen güzel olur
#eğer yapamazsan da bu fonksiyonu downsample'ın içine yaz
def split_nonbuy_data(path):
    file = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/reduced-nonbuy.csv"
    df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',')
    lines = df.iloc[:, 0:4].values
    eval_num = round((lines.shape[0])*0.3)
    rand_num = random.sample(range(0, lines.shape[0]), eval_num)
    dict1 = {}
    for i in range(len(rand_num)):
        dict1[rand_num[i]] = i

    with open("train-nonbuy.csv", "a") as out1, open("eval-nonbuy.csv", "a") as out2:

            for i in range(lines.shape[0]):
                data = lines[i].tolist()
                if i not in dict1:
                    writer1 = csv.writer(out1)
                    writer1.writerow(data)
                else:
                    writer2 = csv.writer(out2)
                    writer2.writerow(data)


# In[6]:


def unique_buy_file(buy_sess_dict):
    dict_to_list = []
    session_count = 0
    for key, value in buy_sess_dict.items():
        session_count += 1
        temp = [key, value]
        dict_to_list.append(temp)
    
    df = pd.DataFrame(dict_to_list)
    df.to_csv("unique-buy.csv", header=False, index=False)
    
    return session_count


# In[ ]:


#For now, never run this main. At the end of the project, the principal main function will be this:
#Whenever you add new things to main, update this main function.
if __name__ == '__main__':
    path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data"
    buy_sess_dict       = read_buys_from_file(path)
    
    #Taking 1 million rows at each time to append non-buy sessions at the end of the file
    non_buy_sess_dict   = read_clicks_from_file1(path, buy_sess_dict)  #Buraya bütün read_clicks_from_file'ları yazman gerekebilir.
    non_buy_sess_list   = convertDictToList(non_buy_sess_dict)
    write_to_non_buy(non_buy_sess_list)
    
    non_buy_sess_dict   = read_clicks_from_file2(path, buy_sess_dict)  #Buraya bütün read_clicks_from_file'ları yazman gerekebilir.
    non_buy_sess_list   = convertDictToList(non_buy_sess_dict)
    write_to_non_buy(non_buy_sess_list)
    
    ...
    ...
    ...
    
    non_buy_sess_dict   = read_clicks_from_file34(path, buy_sess_dict)  #Buraya bütün read_clicks_from_file'ları yazman gerekebilir.
    non_buy_sess_list   = convertDictToList(non_buy_sess_dict)
    write_to_non_buy(non_buy_sess_list)
    
    #to create non-buy sessions file. 8740033 unique non-buy sessions
    unique_dictlist = unique_non_buy_sess(path) 
    unique_non_buy_file(unique_dictlist)
    
    #buy_sess_dict is returned from read_buys_from_file() and is used to extract unique buy sessions
    #unique-buy.csv is created.
    unique_buy_file(buy_sess_dict)
    

    
    
    
    '''
    NON-BUY'DAKİ VERİLERİ DOWNSAMPLING YAPARAK AZALT VE ARDINDAN ONLARI DA İKİYE BÖL
    '''

