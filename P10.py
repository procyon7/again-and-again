
# coding: utf-8

# In[1]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv
import collections


# In[2]:


f = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/only-multiples.csv"
pf = pd.read_csv(filepath_or_buffer = f, header=None, sep=',') 
lines_multi = pf.iloc[:,0:4].values


# In[3]:


sess_item = {}

for i in range(lines_multi.shape[0]): 
    inp = lines_multi[i][0]
    val = [lines_multi[i][1],lines_multi[i][2]]
    if inp in sess_item:
        sess_item[inp].append(val) 
    else:
        sess_item[inp] = [val]


# In[4]:


item_list = []
for key in sess_item:
    consec_cl = []
    consec = 0
    item_list = sess_item.get(key)
    item_list.sort()
    for i in range(len(item_list)-1):
        if item_list[i][1] == item_list[i+1][1]:
            consec += 1
        else:
            consec = 0
        
        consec_cl.append(consec)
    sess_item[key] = max(consec_cl)


# In[5]:


f1 = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/p3-p4-p5-p6.csv"
df = pd.read_csv(filepath_or_buffer = f1, header=None, sep=',') 
lines = df.iloc[:,0:8].values

#to compute average value for maximal number of items the session has clicked on the same item
#I need the number of all sessions

all_sess = {}
for i in range(lines.shape[0]):
    if lines[i][0] not in all_sess:
        all_sess[lines[i][0]] = i


# In[6]:


count = 0
for key in sess_item:
    count += sess_item[key]


# In[7]:


avg_consec = count/len(all_sess)


# In[8]:


#birden fazla olan itemlar için consecutive clicks hesaplandı. Eğer 0'sa ortalama yazıldı.
#train datasında tek örneği olan session'lar için de ortalaması yazıldı
#çünkü train data oluşturulurken down-sampling kullandığı için belki bu sessionlar da
#art arda tıkladılar ama biz o datayı dahil etmedik. bu yüzden onlara ortalamayı yazıyorum.
consec_cl = np.empty([lines.shape[0], 9], dtype=object)

for i in range(lines.shape[0]):
    l = np.array(lines[i])
    if lines[i][0] in sess_item:
        if sess_item.get(lines[i][0]) == 0:
            y = np.array(avg_consec)
            consec_cl[i] = np.append(l, y)
        else:
            y = np.array(sess_item.get(lines[i][0]))
            consec_cl[i] = np.append(l, y)
    else:
        y = np.array(avg_consec)
        consec_cl[i] = np.append(l, y)


# In[9]:


with open('p3-p4-p5-p6-p10.csv', 'w') as out:
    for i in range(consec_cl.shape[0]):
        data = consec_cl[i].tolist()
        writer = csv.writer(out)
        writer.writerow(data)

